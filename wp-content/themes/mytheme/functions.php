<?php



/**
* Classic Editor.
*/
define( 'THEME_URL', get_stylesheet_directory() );
define ( 'CORE', THEME_URL . "/core" );
require_once( CORE . "/init.php" );
function init() {
   add_filter('use_block_editor_for_post', '__return_false');
   // add_theme_support( 'woocommerce' ); 

   add_theme_support( 'post-thumbnails' ); 
   /**
   * Register menu.
   */
   register_nav_menu ( 'main-menu', __('Main Menu', 'thachpham') );
   register_nav_menu ( 'footer-menu', __('Footer Menu', 'thachpham') );

   /**
   * Register sidebar.
   */
   if (function_exists('register_sidebar')){
      register_sidebar(array(
      'name'=> 'Sidebar',
      'id' => 'sidebar',
   ));
   }
}
add_action('init', 'init' );


// function add_favicon() {
 
// echo '<link rel="shortcut icon" type="image/png" href="'.get_template_directory_uri().'/assets/icon.png" />';
 
// }
 
// add_action('wp_head', 'add_favicon');


// add_action('wpcf7_before_send_mail', 'save_form' );

// function save_form( $wpcf7 ) {
//    global $wpdb;
   
//    /*
//     Note: since version 3.9 Contact Form 7 has removed $wpcf7->posted_data
//     and now we use an API to get the posted data.
//    */

//    $submission = WPCF7_Submission::get_instance();

//    if ( $submission ) {
    
//        $submited = array();
//        $submited['title'] = $wpcf7->title();
//        $submited['posted_data'] = $submission->get_posted_data();
                
//     }

//      $data = array(
//    		'name'  => $submited['posted_data']['name'],
//    		'email' => $submited['posted_data']['email']
//    	     );

//      $wpdb->insert( $wpdb->prefix . 'tps_forms', 
// 		    array( 
//           'form'  => $submited['title'], 
// 			   'data' => serialize( $data ),
// 			   'date' => date('Y-m-d H:i:s')
// 			)
// 		);
// }

/**
 * get all categories product
 */