<?php global $theme_options; ?>

</div>
<!-- footer -->
    <!-- <footer class="gray-background pt-4 pb-4">
        <div class="container mt-2 text-right">
            <a href="/contact" title="Contact Us">
                <span class="mr-2">We have much more design, shape and color, please
                    contact
                    us</span>
                <i class="fa fa-envelope" style="
                color: #a1c359;
                font-size: 24px;
            " aria-hidden="true"></i> </a>
        </div>
    </footer> -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8">
        <h3>Contact</h3>
        <ul>
          <li><strong>Company name:</strong> <?php echo strtoupper($theme_options['company-name']) ?></li>
          <li><strong>Tax code:</strong> <?php echo $theme_options['tax'] ?></li>
          <li><strong>Address:</strong> <?php echo $theme_options['address'] ?></li>
          <li><strong>Email:</strong> <a href="mailto:<?php echo $theme_options['mail'] ?>"><?php echo $theme_options['mail'] ?></a></li>
          <li><strong>Mobile:</strong> (+84)<?php echo $theme_options['phone-number'] ?></li>
        </ul>
      </div>

      <div class="col-lg-4 col-md-4">
        <h3>Vnature</h3>
        <?php wp_nav_menu( 
            array( 
            'theme_location' => 'footer-menu', 
            'container' => 'false', 
            'menu_id' => 'footer-menu', 
            'menu_class' => ''
        ) 
        ); ?>

      </div>
      
    </div>
  </div>
  <div class="copyright text-center">
    <section id="lab_social_icon_footer">
    <!-- Include Font Awesome Stylesheet in Header -->
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
      <div class="container">
          <div class="text-center center-block">
            <a href="#"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
            <a href="#"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
            <a href="#"><i id="social-gp" class="fa fa-instagram fa-3x social"></i></a>
            <a href="#"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
          </div>
      </div>
    </section>
  <div>
    Copyright &copy; 2020 <span>Vnature Co.,Ltd</span>
  </div>
  </div>
</footer>
<a id="button"></a>

    <?php wp_footer(); ?>
    <script src="<?php echo get_template_directory_uri();  ?>/js/jquery-3.5.1.min.js"> 
    </script>
    <script src="<?php echo get_template_directory_uri();  ?>/js/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous">
    </script>
    <script src="<?php echo get_template_directory_uri();  ?>/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <script src="<?php echo get_template_directory_uri();  ?>/js/script.js">
    </script>
  
</body>

</html>