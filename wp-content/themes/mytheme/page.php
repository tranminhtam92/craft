<?php get_header(); ?>
    <!-- <h1> this is page </h1> -->
    <!-- content -->
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                    $term = get_queried_object();
                    // echo var_dump($term);


                    if ( is_front_page() ) { ?>
                        <!-- <h1> this is Home </h1> -->
                        <?php the_content();
                    } else {
                        // echo '<h1> this is page not Home</h1>';
                        if (is_product_category()){
                            // echo '<h1> this is category </h1>';
                            $term = get_queried_object();
                            if ($term->parent == 0) {
                                echo get_template_part('template-parts/collection');
                            } else {
                                echo get_template_part('template-parts/product');
                            }
                           
                        } else {
                            // echo '<h1> this is list category </h1>';
                            // echo get_template_part('template-parts/category-collection');
                            the_content();
                        }
                    }
                        
                    
                    endwhile; else: ?>
                    <p>Sorry, no posts matched your criteria.</p>
                <?php endif; ?>

    </div>
<?php get_footer(); ?>