<?php get_header(); ?>
    <!-- content -->
    <div class="container mt-2 mb-2">
        <div class="d-flex bd-highlight">
            <?php get_sidebar(); ?>
            <div class="p-2 flex-fill bd-highlight">
                <img data-src-vnn="http://www.ancopottery.com/Main/assets/more/img/page/31/HOME_00.png"
                    data-filename="HOME_00.png" style="width: 100%;" class="mb-4"
                    src="http://www.ancopottery.com/Main/assets/more/img/page/31/HOME_00.png">

                <p
                    style="color: rgb(51, 51, 51); font-family: &quot;Futura Bk BT&quot;; line-height: 1.4;text-align: justify;">
                    <span style="color: rgb(85, 92, 93);"><span class="FuturaXBlkBT"
                            style="font-family: &quot;Futura XBlk BT&quot; !important;">ANCO Company
                            Ltd.</span>&nbsp;</span>- a private own company, located in Binh Duong province - was
                    founded in 1994 with the idea to produce the latest Vietnamese handicrafts products to worldwide
                    customers. </p>
                <p style="color: rgb(51, 51, 51); font-family: &quot;Futura Bk BT&quot;;text-align: justify;">We
                    specialize in indoor &amp; outdoor, home &amp; garden decoration products such as planters,
                    ornaments, furniture...in different sorts of materials: light-cement, fiber cement, polyresin,
                    bamboo, wood…. We are continuously changing assortment, providing a constant stream of new
                    products and surprises for all kinds of customers.</p>
                <p style="color: rgb(51, 51, 51); font-family: &quot;Futura Bk BT&quot;;text-align: justify;">Being
                    one of the largest vietnamese suppliers, with high production capacity, good facility of
                    warehousing and logistic base on international standards of management, quality and win-win
                    thinking, from 25 - year experience in supplying to many importers, wholesalers, retailers
                    worldwide, we firmly believe in providing good products at high quality, reasonable prices to
                    all customers.</p>
                <p style="color: rgb(51, 51, 51); font-family: &quot;Futura Bk BT&quot;;text-align: justify;">Always
                    with our concept to expand our business to reliable customers, we welcome your attention,
                    contact, visit, and working with our company.</p>
            </div>
        </div>
    </div>
<?php get_footer(); ?>