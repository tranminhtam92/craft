<?php
/*
 Template Name: Colecttion Category
 */
get_header();
?>
<?php
global $theme_options;
$args = array(
    'parent' => 0,
    'hide_empty' => false,
    'taxonomy' => 'product_cat',
); 
$categories = get_terms($args);
$contentRows = '';
$colBootrap = 12/$theme_options['collection-category-column-num'];
$i = 1;
foreach ( $categories as $category ) { 
    
    $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
    $image_url = wp_get_attachment_url( $thumbnail_id );
    $title = $category->name;
    $contentRows = sprintf('%s <div id="colection_category_element" class="col-%d"> <a href="%s" title="%s"> 
    <img src="%s"
        class="w-100" />
    <div style="
    background-color:#df8a43;
    text-align: center;
    padding: 10px 2px 10px 2px;
    color: #FFF;
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: 1px;">
        %s
    </div>
    </a> </div> ', $contentRows, $colBootrap, get_term_link($category->term_id, 'product_cat'), $title, $image_url, $title);
    if ($i%$theme_options['collection-category-column-num'] == 0 ) { 
        echo sprintf('<div class="row"> %s </div>', $contentRows); 
        $contentRows = '';
    }
    $i = $i + 1;
} 
echo sprintf('<div class="row"> %s </div>', $contentRows); ?>

<?php get_footer(); ?>