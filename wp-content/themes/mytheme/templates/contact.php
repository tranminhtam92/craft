<?php
/*
 Template Name: Contact
 */
global $theme_options;
get_header();
?>
    <!-- content -->
    <div class="container mt-2 mb-2">
        <div class="ancoContent">
            <div class="contentPage" style="display:table-cell;">
                <div style="display:table">
                    <div class="squareT"><i class="fa fa-stop"></i></div>
                    <h1 class="title">Contact us</h1>
                </div>
                <div><span class="boldF"><strong>Office &amp; Showroom</strong></span><span>: <?php echo $theme_options['address']?></span> </div>
                <div> <span class="boldF"><strong>Mobile</strong></span>: (+84)<span class="HotLine Info"><?php echo $theme_options['phone-number']?></span></div>
                <div><span class="boldF"><strong>Email</strong></span>: <span> <?php echo $theme_options['mail']?> </span></div>

                <div style="display:table">
                    <div class="squareT"><i class="fa fa-stop"></i></div>
                    <h1 class="title">Contact form</h1>
                </div>
                <div class="formContact">
                    <?php echo do_shortcode('[contact-form-7 id="11" title="Form liên hệ 1"]') ;?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>