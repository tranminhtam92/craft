<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();  ?>/css/bootstrap.min.css">
    <link href="<?php echo get_template_directory_uri();  ?>/vendor/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();  ?>/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
global $theme_options;

?>
    <!-- header -->
    <header>
        <div class="header-middle">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">

                        <a href="<?php echo get_bloginfo('url'); ?>" title="Trang chủ">
                            <img src="<?php echo $theme_options['header-logo-image']['url'];?>"
                                class="img-responsive w-240" alt="logo" title="logoTrangChu" style="
                            width: 120px;
                            padding: 5px 0;
                        "> </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <nav class="site-header sticky-top gray-background">
        <div id="main-menu" class="container pt-4 pb-4">
     
            <ul class="nav nav-pills">
            <?php 
                $menuLocations = get_nav_menu_locations(); 
                $menuID = $menuLocations['main-menu']; 
                $primaryNav = wp_get_nav_menu_items($menuID);
                $parentHasSub = array();
                $parentIdActive = 0;

                foreach ( (array)$primaryNav as $navItem ) { 
                    
                   if ($navItem->menu_item_parent != 0) {
                        $active = 'dropdown-item';
                        if ($navItem->object_id == get_queried_object_id()) {
                            $active = 'dropdown-item active';
                            $parentIdActive = $navItem->menu_item_parent;
                        }
                        $subMenu = sprintf('<a class="%s" href="%s">%s</a>', $active, $navItem->url, $navItem->title);
                        if (!array_key_exists($navItem->menu_item_parent, $parentHasSub)) {
                            $parentHasSub[$navItem->menu_item_parent] = $subMenu;
                        } else {
                            $parentHasSub[$navItem->menu_item_parent] = $parentHasSub[$navItem->menu_item_parent].$subMenu;
                        }

                   }
                    
                }

                foreach ( (array)$primaryNav as $key => $navItem ) {
                    if (!$navItem->menu_item_parent) {
                        if (array_key_exists($navItem->ID, $parentHasSub)) {
                            $attClass = 'nav-link dropdown-toggle ml-2';
                            if ($navItem->object_id == get_queried_object_id() || $navItem->ID == $parentIdActive) {
                                $attClass = 'nav-link active dropdown-toggle ml-2';
                            }
                            $aClass = sprintf('<a class="%s" data-toggle="dropdown" href="%s" role="button" aria-haspopup="true" aria-expanded="false">%s</a>', $attClass, $navItem->url, $navItem->title);
                            $subMenu = sprintf('<div class="dropdown-menu"> %s </div>', $parentHasSub[$navItem->ID]);
                            $liClass = sprintf('<li class="nav-item dropdown"> %s %s </li>', $aClass, $subMenu);
                            echo $liClass;
                        } else {
                            $attClass = ( $navItem->object_id == get_queried_object_id() ) ? 'nav-link active ml-2' : 'nav-link ml-2';
                            echo '<li class="nav-item"> <a class="'.$attClass.'" href="'.$navItem->url.'" title="'.$navItem->title.'">'.$navItem->title.'</a> </li>';
                        }
                    }
                    
                }
            ?>
        </div>
    </nav>

    <div class="container mt-2 mb-2">
