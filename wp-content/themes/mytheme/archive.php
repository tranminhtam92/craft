<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();  ?>/css/bootstrap.min.css">
    <link href="<?php echo get_template_directory_uri();  ?>/vendor/fontawesome-free-5.14.0-web/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();  ?>/css/style.css">
</head>

<body>
    <!-- header -->
    <header>
        <div class="header-middle">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">

                        <a href="/" title="Trang chủ">
                            <img src="http://www.ancopottery.com/Main/assets/more/img/logo/5a4ba1b1-a295-49f3-9222-f6b88a3f2d9d.png"
                                class="img-responsive pt-4 pb-4" alt="logo" title="logoTrangChu"> </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <nav class="site-header sticky-top gray-background">
        <div class="container pt-4 pb-4">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a class="nav-link active ml-2" href="#">Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle ml-2" data-toggle="dropdown" href="#" role="button"
                        aria-haspopup="true" aria-expanded="false">About Anco</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link ml-2" href="#">Product</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- content -->
    <div class="container mt-2 mb-2">
        <div class="row">
            <div class="col-6">
                <img src="http://www.ancopottery.com/Main/assets/more/img/menu/8c1b8e68-dfc4-4b84-96a4-76a60e3a48df.jpg"
                    class="w-100" />
                <div style="
                background-color:#df8a43;
                text-align: center;
                padding: 10px 2px 10px 2px;
                color: #FFF;
                text-transform: uppercase;
                font-weight: 700;
                letter-spacing: 1px;">
                    Planter Collection
                </div>
            </div>
            <div class="col-6">
                <img src="http://www.ancopottery.com/Main/assets/more/img/menu/5c2d8eec-c747-4c91-810b-32abbaa5b51f.jpg"
                    class="w-100" />
                <div style="
                background-color:#868686;
                text-align: center;
                padding: 10px 2px 10px 2px;
                color: #FFF;
                text-transform: uppercase;
                font-weight: 700;
                letter-spacing: 1px;">
                    Furniture Collection
                </div>
            </div>
        </div>
    </div>

    <!-- footer -->
    <footer class="gray-background pt-4 pb-4">
        <div class="container mt-2 text-right">
            <a href="/contact" title="Contact Us">
                <span class="mr-2">We have much more design, shape and color, please
                    contact
                    us</span>
                <i class="fa fa-envelope" style="
                color: #a1c359;
                font-size: 24px;
            " aria-hidden="true"></i> </a>
        </div>
    </footer>


    <script src="<?php echo get_template_directory_uri();  ?>/js/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="<?php echo get_template_directory_uri();  ?>/js/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous">
    </script>
    <script src="<?php echo get_template_directory_uri();  ?>/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
</body>

</html>