
<?php
$term = get_queried_object();
$parent = $term->term_id;
if ($term->parent) {
    $parent = $term->parent;
}
$args = array(
    'parent' =>  $parent,
    'hide_empty' => false,
    'taxonomy' => 'product_cat',
); 
$categories = get_terms($args);
$contentRows = '';
foreach ( $categories as $category ) { 
    $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
    $image_url = wp_get_attachment_url( $thumbnail_id );
    $title = $category->name;
    $attActive = ($category->term_id == get_queried_object_id()) ? 'nav-link active' : 'nav-link';
    $contentRows = sprintf('%s <a class="%s" href="%s">%s</a>',
                            $contentRows, $attActive, get_term_link($category->term_id, 'product_cat'), $title);
} 
echo sprintf('<div class="menuleft p-4 mr-2">


                <nav class="nav flex-column">
                    <h3 class="nav-link"> Categories</h3>
                    %s
                </nav>
            </div> ', $contentRows); ?>


 