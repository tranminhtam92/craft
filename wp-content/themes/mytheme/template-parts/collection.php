

<?php
$term = get_queried_object();
?>
<div class="d-flex">
    <?php get_sidebar(); ?>
    <div class="p-2 flex-fill" style="
            background: #454842;
        ">
                <h1 class="text-center position-relative" style="
                color: #a0c080;
                margin: 0;
                margin-top: 10px;
                font-weight: 100;
            "><u style="
            position: absolute;
            border-bottom: 1.4px solid #a0c064;
            text-decoration: none;
            width: 344px;
            bottom: -5px;
            left: 50%;
            transform: translateX(-50%);
                "></u>
                    Product overview</h1>
                <?php 
                    $args = array(
                        'parent' => $term->term_id,
                        'hide_empty' => false,
                        'taxonomy' => 'product_cat',
                    ); 
                    $categories = get_terms($args);
                    $columns = '';
                    $i = 1;
                    foreach ( $categories as $category ) { 
                        $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                        $image_url = wp_get_attachment_url( $thumbnail_id );
                        $columns = sprintf('%s <div class="col-xs-12 col-md-4" id="colection_image" >
                        <div class="position-relative">
                            <a href="%s" title="%s">
                                <img data-u="image"
                                    data-src-vnn="%s"
                                    alt="NotIMG" class="img-fluid"
                                    src="%s">
                                <div class="titleSlider"> %s </div>
                            </a> </div>
                        </div>', $columns, get_term_link($category->term_id, 'product_cat'), $category->name, $image_url, $image_url, $category->name);
                        if ($i%3 == 0 ) { 
                            echo sprintf('<div class="row mt-4"> %s </div>', $columns);
                            $columns = '';
                        }
                        $i = $i + 1;
                    
                    }
                if ($i%3 != 1) {
                    echo sprintf('<div class="row mt-4"> %s </div>', $columns);

                }
                ?>

    </div>
</div>
