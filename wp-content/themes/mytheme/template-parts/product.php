<div class="d-flex">
<?php get_sidebar(); ?>
<div class="flex-fill position-relative">

<?php               
 $args = array(
    'post_type'             => 'product',
    'post_status'           => 'publish',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'id',
            'terms' => get_queried_object_id(),
        )
    ),
    );
    $products = new WP_Query($args);
    global $wp_query; $wp_query->in_the_loop = true;
    $columns = '';
    $i = 1;
    while ($products->have_posts()) : $products->the_post();
        
        global $product;
        $att = ($i == 1)? 'carousel-item active' : 'carousel-item';
        $columns = sprintf('%s <div class="%s"> <img src="%s" class="d-block w-100" alt="%s"> </div>', $columns, $att, wp_get_attachment_url( $product->get_image_id() ), wp_get_attachment_url( $product->get_image_id() ),);
        $i ++ ;
    endwhile; wp_reset_postdata();
    echo sprintf('<div id="carouselExampleControls" class="carousel slide" data-interval="false" data-ride="carousel">
    <div class="carousel-inner"> %s </div> </div>', $columns);
?>

<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <div class="arrowSlider leftVNN"> <span>&lt;</span></div>
</a>
<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <div class="arrowSlider rightVNN"> <span>&gt;</span></div>
</a>
</div>